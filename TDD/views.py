from django.shortcuts import render, redirect
from .forms import PostForm
from .models import PostModel

# Create your views here.
def index(request):
    post_form = PostForm()
    status = PostModel.objects.all()
    if request.method == "POST":
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            post_form.save() 
            return redirect('/')
    
    return render(request, "landing.html", {'post_form':post_form, 'status':status})
