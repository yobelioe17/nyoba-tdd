from django import forms
from . import models

class PostForm(forms.ModelForm):

        pesan = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "input",
                "id" : "status-input",
                "required" : True,
                "placeholder":"Wanna tell me something?",
                "max_length": 300,
                "autocomplete": 'off',
                "style" : "width:300px",
                }))

        class Meta:
                model = models.PostModel
                fields = ["pesan"]
