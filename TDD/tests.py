from django.test import TestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from .models import PostModel
from .forms import PostForm
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):

        #test url '/' exist or not
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        #test url using index view function
    def test_calling_right_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

        #test '/' using templates landing.html
    def test_using_right_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')


        #test creating PostModel model and expecting right value of attribute
    def test_model_created(self):
        PostModel.objects.create(pesan = "Kita Semua orang baik")
        status_objs = PostModel.objects.all()
        self.assertEqual(len(status_objs), 1)
        
        objek = status_objs[0]
        self.assertEqual(objek.pesan, "Kita Semua orang baik")

        self.assertEqual(objek.waktu.minute, timezone.now().minute)
        self.assertEqual(objek.waktu.hour, timezone.now().hour)
        self.assertEqual(objek.waktu.date(), timezone.now().date())

        #test get the model objects
    def test_get_model(self):
        status_obj = PostModel.objects.create(pesan = "Kita Semua orang baik")
        objek = PostModel.objects.get(pesan="Kita Semua orang baik")

        self.assertEqual(status_obj, objek)

       # test form creating and saving, and object created after saving form
    def test_form(self):
        form = PostForm(data={'pesan':"Semoga Lulus"})

        self.assertTrue(form.is_valid())

        form.save()
        status_obj = PostModel.objects.get(id=1)
        self.assertNotEqual(status_obj, None)
        self.assertEqual(status_obj.pesan, form.cleaned_data['pesan'])

        #test the form are rendered in page
    def test_page_contained_form(self):
        response = self.client.get('/')
        self.assertContains(response, PostForm()['pesan'])

        #test post request and saving the request data as PostModel
    def test_post_form(self):
        post_resp = self.client.post('/', data={'pesan':'Semoga Lulus'})
        self.assertEqual(post_resp.status_code, 302)

        status = PostModel.objects.get(id=1)
        self.assertEqual(status.pesan, "Semoga Lulus")

        #test input form and model with some number of charachter (max_length = 300 charachter)
    def test_input_maximum_length_300(self):
        long_text = ''
        for i in range(300):
            long_text += 'a'

        form = PostForm(data={'pesan': long_text})
        self.assertEqual(len(form.data['pesan']), 300)

        post_resp = self.client.post('/', data={'pesan': long_text})
        status_obj = PostModel.objects.get(id=1)
        self.assertEqual(len(status_obj.pesan), 300)

        # try input more than 300 charachter
        long_text+='additional'
        form2 = PostForm(data={'pesan': long_text})
        self.assertFalse(form2.is_valid())

        post_resp = self.client.post('/', data={'pesan': long_text})
        status_objs = PostModel.objects.all()
        self.assertEqual(len(status_objs), 1)

        #test the page will render all the PostModel model data
    def test_rendered_all_status_data(self):
        PostModel.objects.create(pesan="Semoga Lulus")
        response = self.client.get('/')
        self.assertContains(response, 'Semoga Lulus')

        for status in range(3):
            self.client.post('/', data={'pesan': 'status'+str(status)})

        response = self.client.get('/')
        for i in range(3):
            self.assertContains(response, 'status'+str(status))


class FunctionalTest(TestCase):

        #setup selenium browser
    def setUp(self):
        fireFox_options = Options()
        fireFox_options.add_argument('--dns-prefetch-disable')
        fireFox_options.add_argument('--no-sandbox')
        fireFox_options.add_argument('--headless')
        fireFox_options.add_argument('disable-gpu')
        self.browser = webdriver.FireFox()
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()


